## [2.4.9](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.4.8...2.4.9) (2025-02-03)


### :bug: Fixes

* **deps:** update dependency dotenv to v16.4.7 ([b85f647](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/b85f647be9ba1fc42e73d76f57d7dfd3b579ad20))
* **deps:** update dependency express to v4.21.2 ([54ed3ba](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/54ed3bad093fe36f89ba307a7dfe057153d3ee1e))
* **deps:** update dependency pino to v9.6.0 ([0795857](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/07958571147ebf0021a0aa8b8b6dba4aa351a3e0))
* **deps:** update dependency pino-pretty to v12 ([c9e0d13](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/c9e0d13c5fc7a3b620c20ab22ac5fa0c3c034802))
* **deps:** update dependency pino-pretty to v13 ([675f248](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/675f24809840432f56d2910a74d24bea8289c6d4))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency prettier to v3.4.2 ([59c7272](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/59c7272deb9b1c6e670f59ccfa3b89b917c51f75))
* **deps:** update dependency typescript to v5.7.2 ([cd768be](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/cd768bebc03051b95c7580ea93593e4a783085be))
* **deps:** update dependency typescript to v5.7.3 ([33ac89d](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/33ac89d190dee5ec13b1f51fa312edbecdf9441b))
* **deps:** update node.js to v22.12.0 ([f8408e6](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/f8408e69e1636783c1f6f309d0da4eb0d4f8c489))
* **deps:** update node.js to v22.13.1 ([fc013cb](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/fc013cb5a56396abd7ba6ecf29e352502b5a9e3f))
* **deps:** update quay.io/skopeo/stable docker tag to v1.17.0 ([1297f8a](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/1297f8ac99918d82774aadc22e99f98bb604a127))

## [2.4.8](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.4.7...2.4.8) (2024-11-14)


### :bug: Fixes

* **deps:** update dependency dayjs to v1.11.13 ([f5e9b05](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/f5e9b054196f304f57537a8bf60bd366c2f24dc7))
* **deps:** update dependency express to v4.21.1 ([727a9ce](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/727a9cea1d29a837e37ba7a5c8376059e0b58a86))
* **deps:** update dependency pino to v9.5.0 ([55911be](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/55911be1abbad25598613a0f280cffcb8359a2ef))
* **deps:** update dependency pino-pretty to v11.3.0 ([501b915](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/501b91533c1fc5820fd06cd0069d18d1195bee8e))
* **deps:** update dependency superagent to v10 ([395b444](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/395b444b52cf4a98964beadb388de9968c949326))
* **deps:** update dependency superagent to v10.1.0 ([589bd0d](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/589bd0d3a04f2196b702709c61ffddfe4c9a8869))
* **deps:** update dependency superagent to v10.1.1 ([b08e4a2](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/b08e4a2e2af8874c50446006749e08d76ca24252))
* **deps:** update dependency tslib to v2.8.0 ([5cf8ba0](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/5cf8ba0b59eb4377caf4b2f68c7eba3b84f7cabb))
* **deps:** update dependency tslib to v2.8.1 ([78d086b](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/78d086bb4f1236f90457e1ae8a5234009d22aac1))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @types/jest to v29.5.13 ([dcbb702](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/dcbb7022d14aa80b912a23e537f7008e33f06126))
* **deps:** update dependency @types/jest to v29.5.14 ([434e39a](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/434e39a4de69e684c1d5f3235f47daff9ebc5e31))
* **deps:** update dependency @types/superagent to v8.1.9 ([8fbe512](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/8fbe5123d8e3f2e2caec6c91031a670dcb7e6dd3))
* **deps:** update dependency prettier to v3.3.3 ([dc097e3](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/dc097e30077b9d47dd7bb37f2f23f8b537e28df7))
* **deps:** update dependency ts-jest to v29.2.5 ([1058ccc](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/1058ccce810fcb4815ba7d83a8c3a33fd8aa16fe))
* **deps:** update dependency typescript to v5.6.3 ([8e13164](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/8e131648857cdec251802a0f3c6ecfbef9101561))
* **deps:** update node.js to v22.11.0 ([585a751](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/585a751282f92c3da75ae95387c99e0cf4c4bde3))
* **deps:** update node.js to v22.9.0 ([4e78d5e](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/4e78d5ea05f8409b81c103ed35b9ccb2061e1c30))
* **deps:** update quay.io/skopeo/stable docker tag to v1.16.1 ([5c43547](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/5c43547dc9717e5b0ffdbcb0294e3c93a94e9ad0))

## [2.4.7](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.4.6...2.4.7) (2024-08-31)


### :arrow_up: Upgrade dependencies

* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.23.2 ([9bdb984](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/9bdb98400c94089984bfa4c055955f58ebf64faa))
* **deps:** update quay.io/skopeo/stable docker tag to v1.16.0 ([1982eb3](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/1982eb3e1d6afb662f2183b5ef21a07bc383ec87))

## [2.4.6](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.4.5...2.4.6) (2024-06-23)


### :bug: Fixes

* **deps:** update dependency pino to v9.2.0 ([cdaae20](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/cdaae20822fa748601546a290f10f032cf0aead7))
* **deps:** update dependency pino-pretty to v11.2.1 ([82e4e59](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/82e4e598ac90cce9522b7f0644855bfac803dc9b))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency prettier to v3.3.2 ([1dfec98](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/1dfec987939be98b44b0a53fda7f185aefb29e92))
* **deps:** update dependency ts-jest to v29.1.5 ([06b32e2](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/06b32e2d8575b0ba6285e7908217845c20632ea0))
* **deps:** update dependency typescript to v5.5.2 ([4dbaa47](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/4dbaa47693e7ee877821448df72c20bafa562695))
* **deps:** update node.js to v22.3.0 ([193819e](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/193819e53ce643c2528f3c2723b930d3a486f5b0))

## [2.4.5](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.4.4...2.4.5) (2024-06-07)


### :bug: Fixes

* **deps:** update dependency tslib to v2.6.3 ([b3658b7](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/b3658b7e6b2d98f65676497c0c44f976bfbadb24))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency prettier to v3.3.0 ([7b56950](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/7b56950d238e895f91e53766043428b05c83daff))
* **deps:** update dependency prettier to v3.3.1 ([424a52e](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/424a52e0268dc7d9b9b12765639dd1b5722e6d92))
* **deps:** update quay.io/skopeo/stable docker tag to v1.15.1 ([bef36d1](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/bef36d1d530ad505368318cb37d1a277e1075688))

## [2.4.4](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.4.3...2.4.4) (2024-06-01)


### :bug: Fixes

* **deps:** update dependency pino-pretty to v11.1.0 ([07252e5](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/07252e511bfa86e765eb81dd6a06375faaec0aaf))


### :recycle: Refactor

* remove openssl fixed version ([a97d4d9](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/a97d4d9ee111772aa2b3b7d3b70a119a111ca27c))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency ts-jest to v29.1.4 ([9f6ecc6](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/9f6ecc69ff56f775a7ef0dc2b5b394039e19fb13))

## [2.4.3](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.4.2...2.4.3) (2024-05-18)


### :bug: Fixes

* **deps:** update dependency pino to v9.1.0 ([9364c2e](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/9364c2e61fc3d1dd820d2fbb43e34c9d7d057eb1))
* revert workflow ([47846e7](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/47846e7102d522834780aa4285409e399af9af30))


### :construction_worker: CI

*  disable  merge request pipeline ([8064bdd](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/8064bdd860159a20438185b6e37ba7b5279311ca))
* add fake release job ([750b843](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/750b84363584286fe5f3f0fd81be67642d92736d))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency conventional-changelog-conventionalcommits to v8 ([4ff275e](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/4ff275e6a5a162b76a1d0312a7f4028b9b7339d7))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.23.0 ([5181b0f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/5181b0f26236f058d70c6a2125b2f9ed05c630c2))
* **deps:** update node to 22.2.0 ([3324e2e](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/3324e2e6689328d4afe675b8bb54a727ca9c88ed))

## [2.4.2](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.4.1...2.4.2) (2024-05-09)


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @semantic-release/gitlab to v13.1.0 ([2788b9e](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/2788b9e69e471119f7a997b42008201a16eeb577))
* **deps:** update dependency @types/superagent to v8.1.7 ([a234fd5](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/a234fd523bbb67d74fa600560e4bb82afbfa5953))
* **deps:** update dependency conventional-changelog-conventionalcommits to v8 ([03e3ba6](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/03e3ba60c23c28e833cb37ade11ed1b3ee786908))

## [2.4.1](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.4.0...2.4.1) (2024-04-30)


### :bug: Fixes

* **deps:** update dependency dayjs to v1.11.11 ([2e9d012](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/2e9d01270d9c8356889cbbad4d6b0c606a2cfe1d))
* **deps:** update dependency pino to v9 ([be3d7de](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/be3d7de4fd9f95cdbfdda9359a65f3c00006d87f))
* **deps:** update dependency superagent to v9.0.2 ([634520a](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/634520af967d598272119c88f6add32c308fa727))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @semantic-release/gitlab to v13.0.4 ([efdbd80](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/efdbd80f5af6bc6437c6871452d6d4133a5ffeca))

## [2.4.0](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.3.1...2.4.0) (2024-04-27)


### :sparkles: Features

* add tag values ([7061e68](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/7061e689f2742b999cd8a965ee3cd16b89b769bc))


### :bug: Fixes

* **deps:** update dependency pino to v8.21.0 ([0e907ad](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/0e907adc660466e6e9a8dfe583496a16b3e4d120))
* **deps:** update dependency superagent to v9 ([49b22ea](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/49b22ea659d2e44f04f4ac463d3ef6eb387f1fe7))
* license for 2024 ([fd55917](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/fd55917c4aa4f77ef23622a3072c0385a012b0fb))
* remove useless .dockerignore ([d88487e](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/d88487e6df2aff6e8b05271830111e6954b8b3ca))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency supertest to v7 ([1dae379](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/1dae379616d825b2fe2d6b4277a4608d1daa0d9a))

## [2.3.1](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.3.0...2.3.1) (2024-04-21)


### :recycle: Refactor

* add HttpService ([9b82303](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/9b823038b153714949221ed0c49394a0097ba8a8))

## [2.3.0](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.2.2...2.3.0) (2024-04-19)


### :sparkles: Features

* support-utf8-for-title ([58df75a](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/58df75a2987f2c2236a4ccdc79cb9fada499cdbd))


### :arrow_up: Upgrade dependencies

* **deps:** update node.js to v21.7.3 ([f36e3ee](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/f36e3ee9718d0df2b0697231d6dbdbd67535acaf))

## [2.2.2](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.2.1...2.2.2) (2024-04-11)


### :arrow_up: Upgrade dependencies

* **deps:** update dependency typescript to v5.4.5 ([a624d02](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/a624d026f26d513c490b289840ad207fbf77098a))

## [2.2.1](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.2.0...2.2.1) (2024-04-10)


### :bug: Fixes

* **deps:** update dependency pino to v8.20.0 ([25148a6](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/25148a65d82b42cd4280688c23d31afddf2e3e6a))
* openssl set to 3.1.4-r6 ([41b4c61](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/41b4c6154e574271bff2b6d0f4070e34b6713c32))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency typescript to v5.4.4 ([e85d37e](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/e85d37ebcbec531caeab5a6e1f4b08ee68a37b98))
* **deps:** update node.js to v21.7.2 ([108b381](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/108b381d98c2dd34a9f89d8c9debd5bb6771203d))
* **deps:** update python docker tag to v3.12.3 ([d1eeb31](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/d1eeb318b4fe4630b9d5a08a4420c6eaf2474725))
* **deps:** update quay.io/skopeo/stable docker tag to v1.15.0 ([7ee0c2c](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/7ee0c2c7b9d0b31ad2dd9276b196944d251c8a11))

## [2.2.0](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.19...2.2.0) (2024-03-31)


### :sparkles: Features

* new env vars FIRING_STATUS_EMOJI and RESOLVED_STATUS_EMOJI ([b17abe0](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/b17abe01970f36921790e75cc22137e9b48fd0c7))


### :bug: Fixes

* **deps:** update dependency express to v4.19.2 ([40608aa](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/40608aaebc4c6479d0ee6846c27df0a17ed85c74))
* remove console log ([c328132](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/c32813273ace5e4691652330712c29bb925b9730))
* remove useless tags ([21c33fc](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/21c33fc5e961b5945292222d49dee1d48c565213))
* remove useless tags ([99f3df1](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/99f3df1f30eda53ed51d4e4eb69b30f99c2ae610))
* trivy exit code ([a233b5b](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/a233b5b89bf7f3c25612e36a7b260ed52cac7397))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @semantic-release/release-notes-generator to v13 ([dcbf4d8](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/dcbf4d8ab11f4741c15428b9bada50784c2d4cff))
* **deps:** update dependency @types/superagent to v8.1.6 ([1c947c1](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/1c947c1cb86da16a116a60bc4428d0e48aa9e8d6))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.22.0 ([b11127a](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/b11127a8d6cc7756436f0e41afca81646a5a7ea0))

## [2.1.19](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.18...2.1.19) (2024-03-22)


### :bug: Fixes

* **deps:** update dependency express to v4.19.1 ([17697d1](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/17697d1b9ae9b1f8a0b36deec240924cec795cf9))
* **deps:** update dependency pino-pretty to v11 ([e8a5a9f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/e8a5a9f004fcac8348e98c9648a211149e30a84c))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @semantic-release/commit-analyzer to v12 ([a754ffc](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/a754ffcfa015d22494cba643cd277cad740edff1))
* **deps:** update dependency @types/superagent to v8.1.5 ([8be2c27](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/8be2c27983ccdaa589aa78669e4c576c4c431262))
* **deps:** update dependency typescript to v5.4.3 ([5426e4f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/5426e4f0148614b8e81b550682f622892907335f))

## [2.1.18](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.17...2.1.18) (2024-03-12)


### :arrow_up: Upgrade dependencies

* **deps:** update node.js to v21.7.1 ([db6da30](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/db6da305a59054579827153dbd26802d78afa6a1))

## [2.1.17](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.16...2.1.17) (2024-03-08)


### :arrow_up: Upgrade dependencies

* **deps:** update dependency typescript to v5.4.2 ([40d825f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/40d825ffafbe0b55ac48fd305b4c268c61d2c398))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.21.1 ([34ab79a](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/34ab79abdbbec26b837ec57180884ae8def73ab6))
* **deps:** update node.js to v21.7.0 ([f5b9a19](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/f5b9a19329b26e516a8e558633f805a0c2d5afab))

## [2.1.16](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.15...2.1.16) (2024-3-1)


### :bug: Fixes

* **deps:** update dependency express to v4.18.3 ([766ce6c](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/766ce6cd4c3039f79430b7f03dc2677fbf30ff9d))


### :arrow_up: Upgrade dependencies

* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.21.0 ([354aa36](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/354aa3608ec130060a2679c17352741ac3373453))

## [2.1.15](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.14...2.1.15) (2024-2-27)


### :bug: Fixes

* **deps:** update dependency dotenv to v16.4.5 ([1e2ebe4](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/1e2ebe449c835c5f4042a217e6ed18798658b632))
* **deps:** update dependency pino to v8.19.0 ([426a556](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/426a5568b95accd71ba7752607e07b1f79b6e544))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @semantic-release/gitlab to v13.0.3 ([9a16190](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/9a16190493f4349c4efabc8e7027f744327ef38a))
* **deps:** update dependency @types/superagent to v8.1.4 ([32663e2](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/32663e2d9511ae41487f5786bb8a3dd8529c005c))
* **deps:** update dependency nock to v13.5.3 ([75d6b4b](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/75d6b4bb3baa9f224e5fc2f305f875baec07eb55))
* **deps:** update dependency nock to v13.5.4 ([9098b14](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/9098b148b9f267f4fdf5fa715818c9f5d30a98bc))
* **deps:** update quay.io/skopeo/stable docker tag to v1.14.2 ([2edb6df](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/2edb6df5fd90828ea51a56ea7571d513a703ca4a))

## [2.1.14](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.13...2.1.14) (2024-2-16)


### :bug: Fixes

* **deps:** update dependency dotenv to v16.4.2 ([015eb99](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/015eb99a1c71548361567986219c9d981d29055f))
* **deps:** update dependency dotenv to v16.4.4 ([d3328e3](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/d3328e3c38faea17724285ded7453d2d0ae44588))
* **deps:** update dependency pino to v8.18.0 ([247bebd](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/247bebd5427dd6d2d5fee98d8e31668f551dd073))
* remove specific openssl version ([2c046e2](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/2c046e213dc4f3a1e62dd4a11db63bf2b9917298))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @types/jest to v29.5.12 ([1951d7e](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/1951d7effef19ec61f06a70a47d63a684f3849cb))
* **deps:** update dependency prettier to v3.2.5 ([513a82f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/513a82f15131b125ec725471dedb64ed3421e0d1))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.20.1 ([6bfc23d](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/6bfc23d723608c2d40b0303575b0ef849609b84a))
* **deps:** update node.js to v21.6.2 ([3cba202](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/3cba202643eb689a1798af5354a1d586cdd70d8d))
* **deps:** update python docker tag to v3.12.2 ([8dbc252](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/8dbc25200df969d67a8be77b0b4f62f5ec5931b9))

## [2.1.13](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.12...2.1.13) (2024-1-31)


### :arrow_up: Upgrade dependencies

* **deps:** update dependency nock to v13.5.1 ([6a50bcd](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/6a50bcd5719633fb928726ddacb079b787caed33))
* **deps:** update node.js to v21.6.1 ([d9b9699](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/d9b969902f797525c0c80050e5eab3a9b1ec6903))

## [2.1.12](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.11...2.1.12) (2024-1-26)


### :bug: Fixes

* **deps:** update dependency dotenv to v16.4.1 ([4f92ab0](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/4f92ab0ad0aa1099ad5ba43ab42abf4bc9499eb6))
* openssl version ([bd267f6](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/bd267f667778324728d3fe36d83e9178d8e6b73a))
* use --omit=dev ([9bcd192](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/9bcd192ab4f3b249beffe233c4eade4612f57681))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @types/superagent to v8.1.3 ([fb3e4ea](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/fb3e4ea8206dfbbf286522e3596ee7a8ab83a815))

## [2.1.11](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.10...2.1.11) (2024-1-24)


### :bug: Fixes

* **deps:** update dependency dotenv to v16.4.0 ([aadf40f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/aadf40ffefcf6b827f640ee7dda90ad864de79c5))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @types/superagent to v8.1.2 ([f793f4f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/f793f4faa1a10de862661c4770d06c66bed8b8a1))
* **deps:** update dependency ts-jest to v29.1.2 ([777b4c1](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/777b4c1f731eb26c90a885e8a4151c35f75cf85b))

## [2.1.10](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.9...2.1.10) (2024-1-20)


### :bug: Fixes

* **deps:** update dependency dotenv to v16.3.2 ([b39a888](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/b39a8881927aabdac600e4daedc9efdfb8c785a9))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency prettier to v3.2.3 ([2c56457](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/2c56457c7b22164b49da905d29dbfab7b9e6647d))
* **deps:** update dependency prettier to v3.2.4 ([ab29e06](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/ab29e0690e95e8a075ab6f0ae58ce4436c2df5f2))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.20.0 ([ac52d11](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/ac52d1141eefda895498dcbd5186fa500d2feb7e))
* **deps:** update node.js to v21.6.0 ([789aaa0](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/789aaa079621117ed38dc767b37de5d7a376301c))

## [2.1.9](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.8...2.1.9) (2024-1-16)


### :arrow_up: Upgrade dependencies

* **deps:** update dependency nock to v13.5.0 ([a5e6722](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/a5e67228cf8a5a19617566388aef7cfa2aaa07d8))
* **deps:** update dependency node to 21.6.0-alpine ([27d576f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/27d576f6038b437bd2ca7ced992770e266c27a17))
* **deps:** update dependency prettier to v3.2.2 ([4d40990](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/4d409902bbaaa248146416cfbf91a3ecc74adcfb))
* **deps:** update dependency supertest to v6.3.4 ([c25515f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/c25515fe6cca187ac8eaa2de77f322aca5e6dad2))

## [2.1.8](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.7...2.1.8) (2024-1-13)


### :bug: Fixes

* CVE-2023-6129⁠ ([5ca5aba](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/5ca5abae02b30138c4cccb8062b5e62185a6ecd7))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency prettier to v3.2.1 ([3e85a47](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/3e85a47769b532db7cb2d67800a7fb17b6fd6693))

## [2.1.7](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.6...2.1.7) (2024-1-9)


### :bug: Fixes

* **deps:** update dependency pino-pretty to v10.3.1 ([ee79dad](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/ee79dade516d5857b50dba2bbd24534663d057ff))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @semantic-release/gitlab to v13.0.2 ([d87035f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/d87035fb915bda91b263433fb9bd9f9f2ea9ba95))
* **deps:** update dependency @types/supertest to v6.0.2 ([300b556](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/300b5568cca3137b94b44a3f7110d0bde46d29da))

## [2.1.6](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.5...2.1.6) (2023-12-27)


### :bug: Fixes

* **deps:** update dependency pino to v8.17.2 ([3680eed](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/3680eeddd2f3299ac1b367deaeb319ae550e24c0))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @semantic-release/gitlab to v13 ([94fa3c7](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/94fa3c784b3c39fd56fd21c0b92285d27c095ceb))
* **deps:** update dependency @types/superagent to v8 ([3830d3b](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/3830d3b0b339f94b3c4cd939229c3f959ebbd849))
* **deps:** update dependency @types/superagent to v8.1.1 ([871e166](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/871e1669fa7681757e88238797b888002d515f93))
* **deps:** update dependency @types/supertest to v6 ([38665d1](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/38665d1bc70c938d0b8bc1b6271125c591fcda85))

## [2.1.5](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.4...2.1.5) (2023-12-21)


### :bug: Fixes

* **deps:** update dependency pino-pretty to v10.3.0 ([b403f23](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/b403f2372774fdeb5845b8d07dce9d4a69377c4e))


### :arrow_up: Upgrade dependencies

* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.19.2 ([5e2b6f8](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/5e2b6f88a1d54b980bf4317bf62653c1269da151))
* **deps:** update node.js to v21.5.0 ([ba9a165](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/ba9a1652c5191581e412fb1d3ba26346b6bce25a))

## [2.1.4](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.3...2.1.4) (2023-12-15)


### :bug: Fixes

* **deps:** update dependency pino to v8.17.0 ([da4414c](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/da4414c9767b559e44efba778d5e67445d31ca6f))
* **deps:** update dependency pino to v8.17.1 ([2164784](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/21647842b1a4d6d3ddb0f307e7d3c77edf9ee3e7))


### :arrow_up: Upgrade dependencies

* **deps:** Remove fixed version of OpenSSL ([1b8de09](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/1b8de0907f1420b9cd5411b3e9f7665d72ab8623))
* **deps:** update dependency prettier to v3.1.1 ([5167631](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/5167631cfe1edb8c869504b0c0856043b9c3c973))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.19.1 ([fc12636](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/fc12636f0eacabe3a9160b1647f92828dd770774))
* **deps:** update python docker tag to v3.12.1 ([f91453e](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/f91453e1b5d835af65a47de3812340661e8c34ae))

## [2.1.3](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.2...2.1.3) (2023-12-7)


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @types/jest to v29.5.11 ([d0a0c96](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/d0a0c960b9122ecbdac9fb057f1d988b1e8ce57d))
* **deps:** update dependency typescript to v5.3.3 ([deeb432](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/deeb4321a685bad5b70985573f786099e7d12842))
* **deps:** update node.js to v21.4.0 ([71010ca](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/71010ca4f175ff4d8f595fd487e866eb871246e7))
* **deps:** update quay.io/skopeo/stable docker tag to v1.14.0 ([7fedf1d](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/7fedf1dd557bc1126ead9431306780918d3a90c4))

## [2.1.2](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.1...2.1.2) (2023-12-2)


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @types/superagent to v4.1.24 ([341ab2e](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/341ab2e955839a56ade18f408fffa34499b7cb63))
* **deps:** update dependency nock to v13.4.0 ([9ef7312](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/9ef7312d4560998cbb81491ac3e24b57e961e1e4))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.19.0 ([ad5ce7d](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/ad5ce7d84dd53061af0d5ffe6c8eaa0b3098960a))
* **deps:** update node.js to v21.3.0 ([fc16909](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/fc16909af0ed0d5ee7152f5449257bd9560f1f71))

## [2.1.1](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.1.0...2.1.1) (2023-11-22)


### :bug: Fixes

* SonarQube: reliability, maintainability, security review ([8628b7f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/8628b7f2b95f9c03de454acf58f529c01676be49))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @types/jest to v29.5.10 ([66af510](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/66af5109f20450613816508b1e8e6224e2f9222a))

## [2.1.0](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/2.0.0...2.1.0) (2023-11-21)


### :sparkles: Features

* add logger ([aeabad1](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/aeabad146a31d6f3f9b33894b1fef7ea145adc4a))


### :memo: Documentation

* add log level ([13da286](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/13da286789d4e3f44eb659afe39dde499aab262b))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @types/jest to v29.5.9 ([a0d6385](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/a0d6385c90cb2f9c62386594101833a02f9adb3c))
* **deps:** update dependency typescript to v5.3.2 ([52a063a](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/52a063ad09122c169c2d82af76145e8dbe8b8f8e))

## [2.0.0](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/1.2.1...2.0.0) (2023-11-20)


## :mega: **Breaking Change Alert!**

A breaking change has been introduced in this release that requires your attention. Please read the following information carefully to ensure a smooth transition.

### Change Details:

The method for configuring notification priorities has been updated for improved flexibility and consistency. To set notification priorities, you can now choose between two options:

1. **Environment Variable Configuration:**
   If you want to configure notification priorities using environment variables, you will now need to update your configuration. Refer to the [documentation](https://grafana-to-ntfy.cuvellier.fr/installation/environment-variables/) for details on the updated configuration.

2. **Grafana Label Configuration:**
   Alternatively, you can now configure notification priorities using labels in Grafana. Assign specific labels to your alerts to define their priority. For detailed instructions, check the [documentation](https://grafana-to-ntfy.cuvellier.fr/configuration/grafana/rules/#priority).

### :sparkles: Features

* define priority with grafana label ([11ebca0](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/11ebca0c9068ff3ec57e79a6c4d7fc1347e6d2f4))
* default priority can be set by environment variable. ([14d31c1](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/14d31c18995f2aca21165e96b04633a8e4a52328))


### :memo: Documentation

* init CONTRIBUTING.md ([7b28ecf](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/7b28ecf9b4b3f3153ff8dfb706ed817f3c4ffe43))
* init license ([52c09fd](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/52c09fde38bafcb4b7ece87cc684ed2bbbe088ff))


### :white_check_mark: Tests

* add grafana router ([32a10fb](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/32a10fbaba381d648554f8313ef5e81aea2cf6fe))
* initialisation ([d2ba258](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/d2ba2581c79642d86978ecce9414a76fd0cae651))

## [1.2.1](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/1.2.0...1.2.1) (2023-11-18)


### :memo: Documentation

* init MkDocs ([5374ff1](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/5374ff1788566e5ed5e9ec7d320ed51e5cac75a5))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @semantic-release/gitlab to v12.1.1 ([aa46705](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/aa467058ad4b918f658c0841804e174d48efe6fc))
* **deps:** update dependency @types/superagent to v4.1.22 ([1338384](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/13383842b8de1c772a8d95bc5ec3a80433c542c3))

## [1.2.0](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/1.1.4...1.2.0) (2023-11-16)


### :sparkles: Features

* Add panel button ([00e45ee](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/00e45ee26f600ca9749dad57ceb4aa05aa76cabf))


### :arrow_up: Upgrade dependencies

* **deps:** update openssl to 3.1.4-r1 ([6cbdd99](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/6cbdd99b0cc47628a3bc6a3e7683f9573d8d2b10))

## [1.1.4](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/1.1.3...1.1.4) (2023-11-16)


### :memo: Documentation

* add badges ([3d91de0](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/3d91de0cc1a30cdaf63d7cb99425b2af2164004c))


### :construction_worker: CI

* Configure Renovate ([e4db06f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/e4db06ffde81722938581fedb68ebfdcd3b947be))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @semantic-release/gitlab to v12.1.0 ([0cc9e10](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/0cc9e10f38f903321bd7175c77dab11c4dc83d06))
* **deps:** update dependency prettier to v3.1.0 ([9e3cd9c](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/9e3cd9c113071e77a35651adc5bf6a8a3bbd00bc))
* **deps:** update node.js to v21.2.0 ([8f19cb1](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/8f19cb1a00a2709a63a620edff22939ed67779ad))

## [1.1.3](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/1.1.2...1.1.3) (2023-11-13)


### :construction_worker: CI

* kaniko ([9ab0a25](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/9ab0a25d1d02d37b89720a43b85eab00897c9261))
* semantic release ([102cc1f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/102cc1fd3dc7ee48c5f9a830d82c34c2120b57c8))


### :arrow_up: Upgrade dependencies

* **deps:** @types/express @types/superagent prettier typescript superagent tslib ([ef5cdad](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/ef5cdad08a9f7bd9bf30d9b9f5a0c248d14dbaff))
* **deps:** node 21.1.0 ([a413f30](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/a413f303b6d1d4ef5d72496623d09a24d615e11b))

## [1.1.3](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/1.1.2...1.1.3) (2023-11-13)


### :construction_worker: CI

* kaniko ([9ab0a25](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/9ab0a25d1d02d37b89720a43b85eab00897c9261))
* semantic release ([102cc1f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/102cc1fd3dc7ee48c5f9a830d82c34c2120b57c8))


### :arrow_up: Upgrade dependencies

* **deps:** @types/express @types/superagent prettier typescript superagent tslib ([ef5cdad](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/ef5cdad08a9f7bd9bf30d9b9f5a0c248d14dbaff))
* **deps:** node 21.1.0 ([a413f30](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/a413f303b6d1d4ef5d72496623d09a24d615e11b))
