FROM node:22.14.0-alpine AS build
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --ignore-scripts
COPY ./src ./src
COPY ./tsconfig.json ./

RUN npm run build

FROM node:22.14.0-alpine

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --ignore-scripts --omit=dev
COPY --from=build /usr/src/app/dist ./dist

RUN chown -R node:node /usr/src/app
USER node

HEALTHCHECK --interval=30s --timeout=10s --start-period=15s \
  CMD wget -qO- http://localhost:1111/health || exit 1
CMD ["npm", "start"]