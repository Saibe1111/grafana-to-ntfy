<img src="/docs/static/img/logo.png" alt="Logo">

# Grafana to Ntfy

[![Discord](https://img.shields.io/badge/Discord-%235865F2.svg?style=for-the-badge&logo=discord&logoColor=white)](https://discord.gg/qZYJ6fGaZ5)
[![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)](https://hub.docker.com/r/saibe1111/grafana-to-ntfy)
[![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/Saibe1111/grafana-to-ntfy/)

Grafana to Ntfy is a simple application that allows you to send Grafana alerts to Ntfy.

This application is based on [Grafana Webhook](https://grafana.com/docs/grafana/latest/alerting/alerting-rules/manage-contact-points/webhook-notifier/)
and [Ntfy](https://ntfy.sh/) and is written in [Node JS](https://nodejs.org/).

## Documentation

The documentation is available [here](https://grafana-to-ntfy.cuvellier.fr).

## Result

The picture below shows the result of the notification that is sent by this application:

<img src="/docs/static/img/demo.png" alt="Demo">
