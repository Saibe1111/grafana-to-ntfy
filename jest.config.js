/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    collectCoverage: true,
    coverageReporters: ['html', 'text', 'text-summary', 'cobertura', 'lcov'],
    collectCoverageFrom: ['src/**/*.ts', '!src/**/*.spec.ts', '!tests/**/*.ts', '!src/server.ts', '!src/interfaces/**']
};
