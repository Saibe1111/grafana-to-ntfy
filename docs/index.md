<img src="/static/img/logo.png" alt="Logo">

# Grafana to Ntfy

Grafana to Ntfy is a simple application that allows you to send Grafana alerts to Ntfy.

This application is based on [Grafana Webhook](https://grafana.com/docs/grafana/latest/alerting/alerting-rules/manage-contact-points/webhook-notifier/)
and [Ntfy](https://ntfy.sh/) and is written in [Node JS](https://nodejs.org/).

## Result

The picture below shows the result of the notification that is sent by this application:

<img src="/static/img/demo.png" alt="Demo">
