# Alert style with a grafana rule

To setup a rule, you need to go to the `Alerting` tab and click on `New alert rule`.

## Title

The title is the title of the notification.

### Grafana

This is how you can setup the title of the notification:

<img src="/configuration/grafana/rules/grafana/title.png" alt="Grafana title">

### NTFY

Result:

<img src="/configuration/grafana/rules/ntfy/title.jpg" alt="NTFY title">

## Summary

The summary is the summary of the notification.

### Grafana

This is how you can setup the summary of the notification:

<img src="/configuration/grafana/rules/grafana/summary.png" alt="Grafana summary">

### NTFY

Result:

<img src="/configuration/grafana/rules/ntfy/summary.jpg" alt="NTFY summary">

## Labels

The labels are the labels of the notification.

### Grafana

This is how you can setup the labels of the notification:

<img src="/configuration/grafana/rules/grafana/labels-1.png" alt="Grafana labels">

or

<img src="/configuration/grafana/rules/grafana/labels-2.png" alt="Grafana labels">

### NTFY

Result:

<img src="/configuration/grafana/rules/ntfy/labels.jpg" alt="NTFY labels">

## Priority

This is the list of the priorities available and their icons:

!!! warning

    `normal` **is the default notification behavior if the [NTFY_DEFAULT_PRIORITY variable](/installation/environment-variables/) is not defined**

| Priority label | Icon Ntfy                                                                  | Description                                                                                            |
| -------------- | -------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------ |
| critical       | ![min priority](/configuration/grafana/rules/ntfy/priority/priority-5.svg) | Really long vibration bursts, default notification sound with a pop-over notification.                 |
| high           | ![min priority](/configuration/grafana/rules/ntfy/priority/priority-4.svg) | Long vibration burst, default notification sound with a pop-over notification.                         |
| normal         | _(none)_                                                                   | Short default vibration and sound.                                                                     |
| low            | ![min priority](/configuration/grafana/rules/ntfy/priority/priority-2.svg) | No vibration or sound. Notification will not visibly show up until notification drawer is pulled down. |
| min            | ![min priority](/configuration/grafana/rules/ntfy/priority/priority-1.svg) | No vibration or sound. The notification will be under the fold in "Other notifications".               |

### Grafana

This is how you can setup the labels to set the priority of the notification:

<img src="/configuration/grafana/rules/grafana/priority.png" alt="NTFY labels Priority">

### NTFY

Result:

<img src="/configuration/grafana/rules/ntfy/priority.jpg" alt="NTFY buttons">

## Dashboard and panel buttons

The dashboard and panel buttons are the buttons of the notification.

### Grafana

This is how you can setup the dashboard and panel buttons of the notification:

<img src="/configuration/grafana/rules/grafana/dashboard-panel.png" alt="Grafana buttons">

### NTFY

Result:

<img src="/configuration/grafana/rules/ntfy/dashboard-panel.jpg" alt="NTFY buttons">
