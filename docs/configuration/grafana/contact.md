# Add contact point

To add a contact point, you need to go to the `Alerting` tab and click on `Contact points`.
Then you need to click on `New contact point` and select `Webhook`.

The URL is the URL of the `grafana-to-ntfy` application with is `port` and the `/grafana` endpoint.

<img src="/configuration/grafana/contact/webhook.png" alt="Grafana webhook">
