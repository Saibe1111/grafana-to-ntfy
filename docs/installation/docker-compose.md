# Docker compose

If you want to run this project in a docker container with docker-compose, you can use the following snippet:

???+ info

    Don't forget to change the `NTFY_TOPIC` environment variable. And to check all the available environment variables, check the [Environment Variables](/installation/environment-variables/) page.

=== "With Grafana"

    ```yaml
    services:
      grafana:
        container_name: grafana
        image: grafana/grafana
        ports:
          - '3003:3000'

      grafana-to-ntfy:
        container_name: grafana-to-ntfy
        image: saibe1111/grafana-to-ntfy
        restart: unless-stopped
        environment:
          NTFY_TOPIC: grafana-custom-alert-topic
    ```

=== "Without Grafana"

    ```yaml
    services:
      grafana-to-ntfy:
        container_name: grafana-to-ntfy
        image: saibe1111/grafana-to-ntfy
        restart: unless-stopped
        environment:
          NTFY_TOPIC: grafana-custom-alert-topic
    ```
