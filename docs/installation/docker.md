# Docker

If you want to run this project in a docker container, you can use the following command:

???+ tip

    This solution is not the best, but it works.
    I'm recommending to use docker-compose instead.

???+ info

    Don't forget to change the `NTFY_TOPIC` environment variable. And to check all the available environment variables, check the [Environment Variables](/installation/environment-variables/) page.

```bash
docker run --name grafana-to-ntfy -e NTFY_TOPIC=grafana-custom-alert-topic saibe1111/grafana-to-ntfy
```
