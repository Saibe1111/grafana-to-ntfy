# Environment variables

The following environment variables are available:

|     **Variable**      | **Usage**                                                                              |   **Default value**   | **Required** |
| :-------------------: | -------------------------------------------------------------------------------------- | :-------------------: | :----------: |
|      NTFY_TOPIC       | Topic name                                                                             |        grafana        |      ❌      |
|      NTFY_SERVER      | URL of the ntfy server                                                                 |    https://ntfy.sh    |      ❌      |
|      NTFY_TOKEN       | Access tokens [Docs](https://docs.ntfy.sh/config/#access-tokens)                       |           /           |      ❌      |
| NTFY_DEFAULT_PRIORITY | [Set default priority for notifications](/configuration/grafana/rules/#priority)       |        normal         |      ❌      |
|       LOG_LEVEL       | Log level (`trace`/`debug`/`info`/`warn`/`error`/`fatal`/`silent`)                     |         info          |      ❌      |
|  FIRING_STATUS_EMOJI  | Emoji to use for firing status. [NTFY Emoji reference](https://docs.ntfy.sh/emojis/)   |  rotating_light (🚨)  |      ❌      |
| RESOLVED_STATUS_EMOJI | Emoji to use for resolved status. [NTFY Emoji reference](https://docs.ntfy.sh/emojis/) | adhesive_bandage (🩹) |      ❌      |
