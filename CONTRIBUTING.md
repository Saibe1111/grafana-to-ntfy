# Contributing to Grafana To Ntfy

Thank you for considering contributing to Grafana To Ntfy! We appreciate your interest in making our project better.

## Getting Started

1. Fork the repository and clone it to your local machine.
2. Create a new branch for your contribution: `git checkout -b feature/your-feature`.
3. Make your changes and commit them with clear and concise messages.

## Submitting a Contribution

1. Open a new merge request (MR) with a clear title and description.
2. Reference any relevant issues in your MR description using keywords like "Closes #IssueNumber".
3. Ensure that your code is well-documented and follows the project's coding style.

## Code Style and Commit Guidelines

-   Follow the coding style already established in the project.
-   Make commits that are meaningful and adhere to [Semantic Release](https://semantic-release.gitbook.io/semantic-release/) conventions.

## Testing

-   Write unit tests for your code to ensure its correctness.
-   Make sure existing tests pass before submitting a merge request.

## Documentation

-   If your contribution introduces new features or changes existing functionality, update the documentation accordingly.

## Responding to Feedback

Maintainers will review your contribution and may provide feedback or request changes. Be responsive to these requests to facilitate the merging process.

## Thank You

By contributing to Grafana To Ntfy, you agree to abide by the terms of this document.

Thank you for your contribution!
