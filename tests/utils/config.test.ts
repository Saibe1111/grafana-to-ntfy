import { Priority } from '../../src/enum/priority';
import config from '../../src/utils/config';

describe('Config', () => {
    beforeEach(() => {
        jest.resetModules();
    });

    it('should initialize with default values', () => {
        expect(config.ntfyTopic).toBe('grafana');
        expect(config.ntfyServer).toBe('https://ntfy.sh');
        expect(config.ntfyToken).toBeUndefined();
        expect(config.ntfyDefaultPriority).toBe(Priority.DEFAULT);
    });

    it('should set values from environment variables', () => {
        process.env.NTFY_TOPIC = 'customTopic';
        process.env.NTFY_SERVER = 'https://custom.server';
        process.env.NTFY_TOKEN = 'customToken';
        process.env.NTFY_DEFAULT_PRIORITY = 'high';

        config.reset();

        expect(config.ntfyTopic).toBe('customTopic');
        expect(config.ntfyServer).toBe('https://custom.server');
        expect(config.ntfyToken).toBe('customToken');
        expect(config.ntfyDefaultPriority).toBe(Priority.HIGH);
    });
});
