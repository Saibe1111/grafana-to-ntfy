import { Priority } from '../../src/enum/priority';
import { getPriorityHelper } from '../../src/utils/priority';

function testScenario(input: string, expectedOutput: Priority | undefined): void {
    it(`should return ${expectedOutput} for "${input}"`, () => {
        const result = getPriorityHelper(input);
        expect(result).toBe(expectedOutput);
    });
}

describe('getPriorityHelper', () => {
    testScenario('critical', Priority.URGENT);
    testScenario('high', Priority.HIGH);
    testScenario('normal', Priority.DEFAULT);
    testScenario('low', Priority.LOW);
    testScenario('min', Priority.MIN);
    testScenario('unknown', undefined);
});
