import { Priority } from '../../src/enum/priority';
import { NtfyMessage } from '../../src/models/NtfyMessage';
import { GrafanaService } from '../../src/services/GrafanaService';
import { HttpService } from '../../src/services/HttpService';
import config from '../../src/utils/config';
import { mockRequest } from '../utilsForTests/mock-request';

const status = 'firing';
const alertname = 'Watchdog';
const summary = 'Watchdog firing';
const silenceURL = 'http://localhost:3000/silences/1234567890';
const dashboardURL = 'http://localhost:3000/d/1234567890';
const panelURL = 'http://localhost:3000/d/1234567890/panel/1234567890';
let priority: string | undefined;

let mockedRequestBody = {
    status: status,
    commonLabels: {
        alertname: alertname,
        priority_ntfy: priority,
        other: 'value'
    },
    commonAnnotations: {
        summary: summary
    },
    alerts: [
        {
            silenceURL: silenceURL,
            dashboardURL: dashboardURL,
            panelURL: panelURL
        }
    ]
};

jest.mock('../../src/services/HttpService', () => {
    return {
        HttpService: {
            postNtfyMessage: jest.fn()
        }
    };
});

describe('GrafanaService', () => {
    describe('success (status 200)', () => {
        beforeEach(() => {
            const mHttpService = require('../../src/services/HttpService').HttpService;
            mHttpService.postNtfyMessage.mockResolvedValue({ ok: true, status: 200 });
        });

        it('should handle success', async () => {
            let mockedRequestBodyWithoutOtherTag = {
                status: status,
                commonLabels: {
                    alertname: alertname,
                    priority_ntfy: priority
                },
                commonAnnotations: {
                    summary: summary
                },
                alerts: [
                    {
                        silenceURL: silenceURL,
                        dashboardURL: dashboardURL,
                        panelURL: panelURL
                    }
                ]
            };
            const mockedReq = mockRequest(mockedRequestBodyWithoutOtherTag);
            const grafanaService = new GrafanaService();
            await grafanaService.postAlert(mockedReq);

            expect(HttpService.postNtfyMessage).toHaveBeenCalledWith(expect.any(NtfyMessage));
        });
    });

    describe('forbidden (status 403)', () => {
        beforeEach(() => {
            const mHttpService = require('../../src/services/HttpService').HttpService;
            mHttpService.postNtfyMessage.mockResolvedValue({ ok: false, status: 403 });
        });

        it('should handle forbidden', async () => {
            const mockedReq = mockRequest(mockedRequestBody);
            const grafanaService = new GrafanaService();
            try {
                await grafanaService.postAlert(mockedReq);
            } catch (error) {
                expect(error).toEqual(new Error('Forbidden'));
            }
        });
    });

    describe('internal server error (status 500)', () => {
        beforeEach(() => {
            const mHttpService = require('../../src/services/HttpService').HttpService;
            mHttpService.postNtfyMessage.mockResolvedValue({ ok: false, status: 500 });
        });

        it('should handle forbidden', async () => {
            const mockedReq = mockRequest(mockedRequestBody);
            const grafanaService = new GrafanaService();
            try {
                await grafanaService.postAlert(mockedReq);
            } catch (error) {
                expect(error).toEqual(new Error('Unpublishable'));
            }
        });
    });

    describe('priority and tags', () => {
        const testScenario = async (status: string, priority: string | undefined, expectedPriority: Priority, expectedTags: string) => {
            mockedRequestBody.status = status;
            mockedRequestBody.commonLabels.priority_ntfy = priority;
            const mockedReq = mockRequest(mockedRequestBody);
            const grafanaService = new GrafanaService();
            await grafanaService.postAlert(mockedReq);

            if (priority) {
                priority = `,${priority}`;
            } else {
                priority = '';
            }
            const ntfyMessage = new NtfyMessage(
                alertname,
                summary,
                expectedPriority,
                expectedTags ? `other=value,${expectedTags}` : 'other=value',
                `view, Silence, ${silenceURL}, clear=true;view, Dashboard, ${dashboardURL}, clear=true;view, Panel, ${panelURL}, clear=true;`
            );
            expect(HttpService.postNtfyMessage).toHaveBeenCalledWith(ntfyMessage);
        };

        beforeEach(() => {
            config.reset();
            mockedRequestBody.status = 'firing';
            const mHttpService = require('../../src/services/HttpService').HttpService;
            mHttpService.postNtfyMessage.mockResolvedValue({ ok: true, status: 200 });
        });

        it('default', async () => {
            await testScenario('other', undefined, Priority.DEFAULT, '');
        });

        it('low as default', async () => {
            config.ntfyDefaultPriority = Priority.LOW;
            await testScenario('other', undefined, Priority.LOW, '');
        });

        it('other', async () => {
            await testScenario('other', undefined, Priority.DEFAULT, '');
        });

        it('critical', async () => {
            await testScenario('firing', 'critical', Priority.URGENT, 'rotating_light');
        });

        it('high', async () => {
            await testScenario('firing', 'high', Priority.HIGH, 'rotating_light');
        });

        it('normal', async () => {
            await testScenario('firing', 'normal', Priority.DEFAULT, 'rotating_light');
        });

        it('low', async () => {
            await testScenario('firing', 'low', Priority.LOW, 'rotating_light');
        });

        it('min', async () => {
            await testScenario('firing', 'min', Priority.MIN, 'rotating_light');
        });

        it('resolved', async () => {
            await testScenario('resolved', undefined, Priority.MIN, 'adhesive_bandage');
        });
    });
});
