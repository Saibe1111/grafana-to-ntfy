import superagent from 'superagent';
import { NtfyMessage } from '../../src/models/NtfyMessage';
import { HttpService } from '../../src/services/HttpService';
import { Priority } from '../../src/enum/priority';
import config from '../../src/utils/config';

jest.mock('superagent', () => ({
    post: jest.fn().mockReturnThis(),
    set: jest.fn().mockReturnThis(),
    send: jest.fn().mockResolvedValue({ ok: true, status: 200 })
}));

describe('HttpService', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should send a POST request to the Ntfy server', async () => {
        const ntfyMessage = new NtfyMessage('title', 'message', Priority.DEFAULT, 'tags', 'actions');
        const result = await HttpService.postNtfyMessage(ntfyMessage);

        expect(superagent.post).toHaveBeenCalledTimes(1);
        expect(superagent.post).toHaveBeenCalledWith(config.ntfyServer + '/' + config.ntfyTopic);
        expect(result).toEqual({ ok: true, status: 200 });
    });
});
