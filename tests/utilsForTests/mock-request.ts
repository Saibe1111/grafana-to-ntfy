import { Request } from 'express';

export const mockRequest = (body: any = {}): Request => {
    return {
        body
    } as Request;
};
