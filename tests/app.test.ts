import supertest from 'supertest';
import app from '../src/app';

describe('app', () => {
    it('should respond with 404 for unknown routes', async () => {
        const response = await supertest(app).get('/nonexistent-route');

        expect(response.status).toBe(404);
    });
});
