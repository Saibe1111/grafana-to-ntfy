import express, { Application } from 'express';
import request from 'supertest';
import { HealthController } from '../../src/controllers/HealthController';

describe('HealthController', () => {
    let healthController: HealthController;
    const app: Application = express();

    beforeAll(() => {
        healthController = new HealthController();
        app.get('/health', healthController.getHealth.bind(healthController));
    });

    it('should return a 200 status and "Server is healthy!" message', async () => {
        const response = await request(app).get('/health');

        expect(response.status).toBe(200);
        expect(response.text).toBe('Server is healthy!');
    });
});
