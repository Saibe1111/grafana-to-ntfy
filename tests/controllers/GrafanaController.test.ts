import express, { Application } from 'express';
import request from 'supertest';
import { GrafanaController } from '../../src/controllers/GrafanaController';
import { GrafanaService } from '../../src/services/GrafanaService';

describe('GrafanaController', () => {
    let grafanaController: GrafanaController;
    let app: Application;

    beforeAll(() => {
        grafanaController = new GrafanaController();
        app = express();
        app.post('/post-alert', grafanaController.postAlert.bind(grafanaController));
    });

    it('should return a 200 status on successful alert posting', async () => {
        jest.spyOn(GrafanaService.prototype, 'postAlert').mockResolvedValueOnce(undefined);
        const response = await request(app).post('/post-alert');
        expect(response.status).toBe(200);
    });

    it('should return a 500 status on error during alert posting', async () => {
        jest.spyOn(GrafanaService.prototype, 'postAlert').mockRejectedValue(new Error('Forbidden'));
        const response = await request(app).post('/post-alert');
        expect(response.status).toBe(500);
        expect(response.body).toEqual({ error: 'Forbidden' });
    });
});
