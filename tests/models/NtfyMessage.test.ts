import { NtfyMessage } from '../../src/models/NtfyMessage';
import { Priority } from '../../src/enum/priority';
import config from '../../src/utils/config';

describe('NtfyMessage', () => {
    it('constructor', async () => {
        const title = `=?UTF-8?B?${Buffer.from('title').toString('base64')}?=`;
        const message = 'message';
        const tags = 'tags';
        const actions = 'actions';

        const expectedHeaders: { [key: string]: string } = {
            priority: 'low',
            title: title,
            tags: tags,
            actions: actions,
            authorization: 'Bearer test'
        };
        config.ntfyToken = 'test';

        let ntfyMessage = new NtfyMessage('title', message, Priority.LOW, tags, actions);
        const messageData = ntfyMessage.getMessageData();

        expect(messageData).toStrictEqual({
            header: expectedHeaders,
            message: message
        });
    });
});
