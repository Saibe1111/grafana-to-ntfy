import { Request, Response } from 'express';
import { GrafanaService } from '../services/GrafanaService';
import log from '../utils/logger';

const grafanaService = new GrafanaService();

export class GrafanaController {
    public async postAlert(req: Request, res: Response): Promise<Response> {
        try {
            log.info('Received message from Grafana.');
            log.trace('Request body: ' + JSON.stringify(req.body));
            await grafanaService.postAlert(req);
            return res.status(200).send();
        } catch (err) {
            const error = err as Error;
            log.error('An error occurred during the transmission of the message. Error:' + error.message);
            return res.status(500).json({ error: error.message });
        }
    }
}
