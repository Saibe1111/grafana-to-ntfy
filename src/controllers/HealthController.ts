import { Request, Response } from 'express';

export class HealthController {
    public async getHealth(_: Request, res: Response): Promise<Response> {
        return res.status(200).send('Server is healthy!');
    }
}
