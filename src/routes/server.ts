import { Router } from 'express';
import { healthRouter } from './healthRouter';
import { grafanaRouter } from './grafanaRouter';

export const indexRouter: Router = Router();

indexRouter.use('/health', healthRouter);
indexRouter.use('/grafana', grafanaRouter);
