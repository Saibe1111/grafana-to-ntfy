import { Request, Response, Router } from 'express';
import { HealthController } from '../controllers/HealthController';

export const healthRouter: Router = Router();
const heathController: HealthController = new HealthController();

healthRouter.get('/', (req: Request, res: Response) => {
    heathController.getHealth(req, res);
});
