import { Request, Response, Router } from 'express';
import { GrafanaController } from '../controllers/GrafanaController';

export const grafanaRouter: Router = Router();
const grafanaController: GrafanaController = new GrafanaController();

grafanaRouter.post('/', (req: Request, res: Response) => {
    grafanaController.postAlert(req, res);
});
