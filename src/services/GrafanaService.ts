import { Request } from 'express';
import { NtfyMessage } from '../models/NtfyMessage';
import { Priority } from '../enum/priority';
import { getPriorityHelper } from '../utils/priority';
import config from '../utils/config';
import log from '../utils/logger';
import { HttpService } from './HttpService';

export class GrafanaService {
    public async postAlert(req: Request): Promise<void> {
        const messagePriotity = getPriority(req.body.status, req.body.commonLabels.priority_ntfy);
        const messageTitle = req.body.commonLabels.alertname;
        const messageDescription = req.body.commonAnnotations.summary;
        const messageTags = getTags(req.body.commonLabels, req.body.status);
        const messageActions = getActions(req.body.alerts[0].silenceURL, req.body.alerts[0].dashboardURL, req.body.alerts[0].panelURL);
        const message = new NtfyMessage(messageTitle, messageDescription, messagePriotity, messageTags, messageActions);

        log.debug(`Trying to publish the alert '${messageTitle}' to Ntfy.`);
        log.trace('Message: ' + JSON.stringify(message));
        //const response = await message.publish();
        const published = await HttpService.postNtfyMessage(message);
        log.info(`The alert '${messageTitle}' was published to Ntfy.`);

        if (!published.ok) {
            if (published.status == 403) {
                throw new Error('Forbidden');
            }
            throw new Error('Unpublishable');
        }
    }
}

function getPriority(status: string, priority: string): Priority {
    if (status == 'resolved') {
        return Priority.MIN;
    }

    const userDefinedDefaultPriority = config.ntfyDefaultPriority;

    let ntfyPriority = getPriorityHelper(priority);
    if (ntfyPriority) {
        return ntfyPriority;
    }

    return userDefinedDefaultPriority;
}

function getTags(commonLabels: any, status: string): string {
    const keysToRemove = ['alertname', 'priority_ntfy', 'grafana_folder'];
    const filteredKeysObj = Object.fromEntries(Object.entries(commonLabels).filter(([key]) => !keysToRemove.includes(key)));
    const definedValues = Object.entries(filteredKeysObj)
        .filter(([_, value]) => value !== undefined)
        .map(([key, value]) => `${key}=${value}`);
    const str = definedValues.join(',');

    switch (status) {
        case 'firing':
            return str.length > 0 ? str + ',' + config.firingStatusEmoji : config.firingStatusEmoji;
        case 'resolved':
            return str.length > 0 ? str + ',' + config.resolvedStatusEmoji : config.resolvedStatusEmoji;
        default:
            return str;
    }
}

function getActions(silenceURL: string, dashboardURL: string, panelURL: string): string {
    let value = 'view, Silence, ' + silenceURL + ', clear=true;';
    if (dashboardURL) {
        value += 'view, Dashboard, ' + dashboardURL + ', clear=true;';
    }
    if (panelURL) {
        value += 'view, Panel, ' + panelURL + ', clear=true;';
    }
    return value;
}
