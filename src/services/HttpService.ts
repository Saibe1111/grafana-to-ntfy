import superagent from 'superagent';
import { NtfyMessage } from '../models/NtfyMessage';
import config from '../utils/config';

export class HttpService {
    public static async postNtfyMessage(ntfyMessage: NtfyMessage): Promise<{ ok: Boolean; status: Number }> {
        const messageData = ntfyMessage.getMessageData();
        const response = await superagent
            .post(config.ntfyServer + '/' + config.ntfyTopic)
            .set(messageData.header)
            .send(messageData.message);
        return { ok: response.ok, status: response.status };
    }
}
