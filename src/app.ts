import express, { Application } from 'express';
import { indexRouter } from './routes/server';
import * as dotenv from 'dotenv';
dotenv.config();

const app: Application = express();
app.disable('x-powered-by');

app.use(express.json());

app.use('/', indexRouter);

export default app;
