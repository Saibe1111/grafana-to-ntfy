import { MessageHeader } from '../interfaces/MessageHeader';
import { Priority } from '../enum/priority';
import config from '../utils/config';

export class NtfyMessage {
    private header: MessageHeader;
    private message: string;

    constructor(title: string, message: string, priority: Priority, tags: string, actions: string) {
        this.header = {
            priority: priority,
            title: `=?UTF-8?B?${Buffer.from(title).toString('base64')}?=`,
            tags: tags,
            actions: actions,
            authorization: ''
        };

        if (config.ntfyToken) {
            this.header.authorization = 'Bearer ' + config.ntfyToken;
        }

        this.message = message;
    }

    private transformMessageHeaderToObj(header: MessageHeader): { [key: string]: string } {
        const obj: { [key: string]: string } = {};
        for (const key in header) {
            if (header.hasOwnProperty(key)) {
                obj[key] = String(header[key as keyof typeof header]);
            }
        }
        return obj;
    }

    public getMessageData(): { header: { [key: string]: string }; message: string } {
        return {
            header: this.transformMessageHeaderToObj(this.header),
            message: this.message
        };
    }
}
