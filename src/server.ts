import config from './utils/config';
import log from './utils/logger';
import app from './app';

const PORT = 1111;
app.listen(PORT, () => {
    log.info(`The server listens on the port: ${PORT}`);
    log.info(`All alerts will be forwarded to: ${config.ntfyServer}/${config.ntfyTopic}`);
    printConfig();
});

function printConfig(): void {
    log.info('========= Config ==========');
    log.info(`LOG_LEVEL: ${config.logLevel}`);
    log.info(`NTFY_TOPIC: ${config.ntfyTopic}`);
    log.info(`NTFY_SERVER: ${config.ntfyServer}`);
    log.info(`NTFY_TOKEN: ${hideSensitiveInfo(config.ntfyToken)}`);
    log.info(`NTFY_DEFAULT_PRIORITY: ${config.ntfyDefaultPriority}`);
    log.info('====== End of config ======');
}

function hideSensitiveInfo(value: string | undefined): string | undefined {
    return value ? '*'.repeat(value.length) : undefined;
}
