import logger from 'pino';
import dayjs from 'dayjs';
import config from './config';

const log = logger({
    transport: {
        target: 'pino-pretty'
    },
    level: config.logLevel,
    base: {
        pid: false
    },
    timestamp: () => `,"time":"${dayjs().format('DD-MM-YYYY HH:mm:ss')}"`
});

export default log;
