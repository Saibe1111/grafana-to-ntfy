import dotenv from 'dotenv';
import { Priority } from '../enum/priority';
import { getPriorityHelper } from './priority';

/* istanbul ignore next */
if (process.env.NODE_ENV !== 'test') {
    dotenv.config();
}

class Config {
    private defaultPriority: Priority = Priority.DEFAULT;
    private defaultTopic: string = 'grafana';
    private defaultServer: string = 'https://ntfy.sh';
    private defaultLogLevel: string = 'info';
    private defaultFiringStatusEmoji: string = 'rotating_light';
    private defaultResolvedStatusEmoji: string = 'adhesive_bandage';

    public ntfyTopic: string = this.defaultTopic;
    public ntfyServer: string = this.defaultServer;
    public ntfyToken: string | undefined;
    public ntfyDefaultPriority: Priority = this.defaultPriority;
    public logLevel: string = this.defaultLogLevel;
    public firingStatusEmoji: string = this.defaultFiringStatusEmoji;
    public resolvedStatusEmoji: string = this.defaultResolvedStatusEmoji;

    constructor() {
        this.setValues();
    }

    public reset(): void {
        this.setValues();
    }

    private setValues(): void {
        this.ntfyTopic = (process.env.NTFY_TOPIC as string) ?? this.defaultTopic;
        this.ntfyServer = (process.env.NTFY_SERVER as string) ?? this.defaultServer;
        this.ntfyToken = (process.env.NTFY_TOKEN as string) ?? undefined;
        this.ntfyDefaultPriority = getPriorityHelper(process.env.NTFY_DEFAULT_PRIORITY as string) ?? this.defaultPriority;
        this.logLevel = (process.env.LOG_LEVEL as string) ?? this.defaultLogLevel;
        this.firingStatusEmoji = (process.env.FIRING_STATUS_EMOJI as string) ?? this.defaultFiringStatusEmoji;
        this.resolvedStatusEmoji = (process.env.RESOLVED_STATUS_EMOJI as string) ?? this.defaultResolvedStatusEmoji;
    }
}

export default new Config();
