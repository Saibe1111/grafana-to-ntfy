import { Priority } from '../enum/priority';

export function getPriorityHelper(priority: string): Priority | undefined {
    switch (priority) {
        case 'critical':
            return Priority.URGENT;
        case 'high':
            return Priority.HIGH;
        case 'normal':
            return Priority.DEFAULT;
        case 'low':
            return Priority.LOW;
        case 'min':
            return Priority.MIN;
        default:
            return undefined;
    }
}
